(function ($) {
	Drupal.behaviors.taxo_matrix = {
	  attach: function (context, settings) {

	  	$('a.taxo-term', context).click(function(e){
	  		e.preventDefault();
	  	});

	  	$('form.node-form .field-widget-term-matrix a', context).click(function(e){
	  		e.preventDefault();
	  		var selk = $(this).prev().children('input');
	  		if (selk.is(':checked')) {
	  			selk.attr('checked', false);
	  		}
	  		else {
	  			selk.attr('checked',true);
	  		}
	  		$(this).toggleClass('chosen');
	  	});

	  }
	};
}(jQuery));