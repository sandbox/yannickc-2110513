<?php
// $Id

/**
 * @file
 * Configuration pages for taxonomy matrix field
 */

/**
 * Settings for for the term matrix
 */
function taxo_matrix_field_settings_form_text($field, $instance, $has_data) {
  drupal_add_js(drupal_get_path('module', 'taxo_matrix') .'/js/taxo_matrix.js');
  drupal_add_css(drupal_get_path('module', 'taxo_matrix') .'/css/taxo_matrix.css');
  $settings = (array)$field['settings'];
  $form['row'] = array(
    '#type' => 'select',
    '#title' => t('Vocabulary as row'),
		'#options' => voc_list(),
    '#required' => TRUE,
    '#default_value' => $settings['row'],
  );
  $form['col'] = array(
    '#type' => 'select',
    '#title' => t('Vocabulary as column'),
		'#options' => voc_list(),
    '#required' => TRUE,
    '#default_value' => $settings['col'],
  );
  // $form['checkbtn'] = array(
  //   '#type' => 'radios',
  //   '#title' => t('Choose your checkbox button image'),
  //   '#default_value' => $settings['checkbtn'],
  //   '#options' => taxo_matrix_image_options(),
  // );
  return $form;
}

/**
 * Help function. Provides background image options to formatters.
 */
// function taxo_matrix_image_options() {
//   $image_base_url = $GLOBALS['base_path'] . drupal_get_path('module', 'taxo_matrix') . '/images/';
//   $attributes = array('style' => 'margin-top:10px;float:right;');
//   $dir = getcwd() . '/' . drupal_get_path('module', 'taxo_matrix') . '/images/';

//   $options = array();

//   if (($imgs = scandir($dir)) != FALSE) {
//     foreach ($imgs as $img) {
//       if (preg_match('/^[a-zA-Z0-9]+\.[a-zA-Z]{3,4}$/', $img)) {
//         $path_parts = pathinfo($img);
//         $filename = $path_parts['filename'];
//         $options[$filename] = theme('image', array(
//                                               'path' => $image_base_url . $img,
//                                               'alt' => $filename,
//                                               'title' => $filename,
//                                               'width'=>'25px',
//                                               'height' =>'25px',
//                                               'attributes' => $attributes
//                                             )
//         );
//       }
//     }
//   }
//   return $options;
// }


/**
 * Function to generate list of taxonomy vocabularies
 * @return array The list of vocabularies created on the site.
 */
function voc_list() {
	$voc_list = array();
	$vocs = taxonomy_get_vocabularies();
	foreach ($vocs as $voc) {
		$voc_list[$voc->vid] = $voc->name;
	}
	return $voc_list;
}











