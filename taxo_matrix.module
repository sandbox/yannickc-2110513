<?php
// $Id: $

/**
 * @file
 * Defines taxonomy term matrix field types.
 */

/**
 * Implementation of hook_theme()
 */
function taxo_matrix_theme() {
  $theme['taxo_matrix_table'] = array(
    'render element' => 'form',
  );
  $theme['taxo_matrix_output'] = array(
    'variables' => array('header' => NULL, 'rows' => NULL),
  );
  $theme['taxo_matrix_list'] = array(
    'variables' => array('list' => NULL),
  );
  return $theme;
}

/**
 * Implements hook_field_info().
 */
function taxo_matrix_field_info() {
  return array(
    'term_matrix' => array(
      'label' => t('Term reference (Matrix)'),
      'description' => t('Creates a grid of terms.'),
      'settings' => array('row' => '', 'col' => '', 'checkbtn' => ''),
      'default_widget' => 'term_matrix',
      'default_formatter' => 'taxo_matrix_default',
    ),
  );
}

/**
 * Implements hook_field_settings_form().
 */
function taxo_matrix_field_settings_form($field, $instance, $has_data) {
  module_load_include('inc', 'taxo_matrix', 'taxo_matrix.admin');
  return taxo_matrix_field_settings_form_text($field, $instance, $has_data);
}

/**
 * Implementation of hook_content_is_empty().
 */
function taxo_matrix_field_is_empty($item, $field) {
  if (is_array($item) && array_key_exists('value', $item)) {
    if (empty($item['value']) && (string) $item['value'] !== '0') {
      return TRUE;
    }
    return FALSE;
  }
  return TRUE;
}

/**
 * Implements hook_field_widget_info().
 */
function taxo_matrix_field_widget_info() {
  return array(
    'term_matrix' => array(
      'label' => t('Term Matrix'),
      'description' => t('A grid of terms'),
      'field types' => array('term_matrix'),
      'settings' => array(),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_CUSTOM,
        'default value' => FIELD_BEHAVIOR_NONE,
      ),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function taxo_matrix_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $field_name = $field['field_name'];

  $coldata = taxo_matrix_get_voc_details($field['settings']['col']);
  $rowdata = taxo_matrix_get_voc_details($field['settings']['row']);

  $cols_count = $coldata['count'];
  $rows_count = $rowdata['count'];

  $default_values = array();
  foreach ($items as $delta => $item) {
    if($item && !empty($item['row']))
    $default_values[$item['row']][$item['col']][] = $item['row_value'] . '-' . $item['col_value'];
  }

  $element['#taxo_matrix_rows'] = $rows_count;
  $element['#taxo_matrix_cols'] = $cols_count;

  $row_labels = $rowdata['labels'];
  $column_labels = $coldata['labels'];

  for ($row = 1; $row <= $rows_count; $row++) {
    for ($col = 1; $col <= $cols_count; $col++) {
    	$defval = '';
    	$suffix = '<a class="taxo-term" href="#">@</a>';
    	if (isset($default_values[$row][$col])) {
    		if ($default_values[$row][$col][0] != '0-0') {
    			$suffix = '<a class="taxo-term chosen" href="#">@</a>';
    		}
    		$defval = $default_values[$row][$col][0];
    	}
      $element['grid'][$row .'-'. $col] = array(
        '#type' => 'checkbox',
        '#default_value' =>  $defval,
        '#return_value' => $rowdata['tids'][$row-1] . '-' . $coldata['tids'][$col-1],
        '#suffix' => $suffix,
      );
    }
  }

  drupal_add_js(drupal_get_path('module', 'taxo_matrix') .'/js/taxo_matrix.js');
  drupal_add_css(drupal_get_path('module', 'taxo_matrix') .'/css/taxo_matrix.css');

  $element['#theme'] = 'taxo_matrix_table';
  $element['#row_labels'] = $row_labels;
  $element['#column_labels'] = $column_labels;
  $element['#element_validate'] = array('taxo_matrix_field_widget_validate');
  
  return $element;
}

/**
 * Validation handler for taxonomy matrix field handler
 */
function taxo_matrix_field_widget_validate($element, &$form_state) {
  if (isset($form_state['values']['op'])) {
    $rows_count = $element['#taxo_matrix_rows'];
    $cols_count = $element['#taxo_matrix_cols'];
    $delta = 0;
    for ($row = 1; $row <= $rows_count; $row++) {
      for ($col = 1; $col <= $cols_count; $col++) {
        if (!isset($element['grid'][$row .'-'. $col]['#value'])) { //if value is not set
          $value = '';
        }
        elseif(is_array($element['grid'][$row .'-'. $col]['#value']) && empty($element['grid'][$row .'-'. $col]['#value'])) { //if array and value not set
          $value = '';
        }
        else {
          $value = $element['grid'][$row .'-'. $col]['#value']; //value set
        }
        if(is_array($value)) {
          foreach ($value as $val) {
          	list($r, $c) = explode('-', $val);
            $items[$delta] = array(
              'row' => $row,
              'col' => $col,
              'value' => $value,
              'row_value' => $r,
              'col_value' => $c,
            );
            $delta++;
          }
        }
        else {
        	if (strlen($value) > 1) {
	        	list($r, $c) = explode('-', $value);
	          $items[$delta] = array(
	            'row' => $row,
	            'col' => $col,
	            'value' => $value,
	            'row_value' => $r,
	            'col_value' => $c,
	          );
	          $delta++;
        	}
        	else {
	          $items[$delta] = array(
	            'row' => $row,
	            'col' => $col,
	            'value' => $value,
	            'row_value' => $value,
	            'col_value' => $value,
	          );
	          $delta++;
        	}
        }
      }
    }
    form_set_value($element, $items, $form_state);
  }
}

/**
 * Implements hook_field_formatter_info().
 */
function taxo_matrix_field_formatter_info() {
  return array(
    'taxo_matrix_grid' => array(
      'label' => t('Grid'),
      'field types' => array('term_matrix'),
    ),
    'taxo_matrix_list' => array(
      'label' => t('List'),
      'field types' => array('term_matrix'),
      'settings' => array('group_by' => 'col', 'group_prefix_text' => 'Le', 'grp_connector' => 'a'),
    ),
  );
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function taxo_matrix_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $element = array();

  if (strpos($display['type'], '_list') !== FALSE) {
	  $element['group_by'] = array(
	    '#title' => t('Group by'),
	    '#type' => 'select',
	    '#default_value' => $settings['group_by'],
	    '#options' => array('col' => t('Column'), 'row' => t('Row')),
	    '#empty_option' => t('None (No grouping)'),
	  );
	  $element['group_prefix_text'] = array(
	    '#title' => t('Group prefix text'),
	    '#type' => 'textfield',
	    '#size' => 10,
	    '#default_value' => $settings['group_prefix_text'],
	  );
	  $element['grp_connector'] = array(
	    '#title' => t('Group connector text'),
	    '#type' => 'textfield',
	    '#size' => 10,
	    '#default_value' => $settings['grp_connector'],
	  );
  }

  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function taxo_matrix_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $options = array('col' => t('Column'), 'row' => t('Row'));

  $summary = '';

  if (strpos($display['type'], '_list') !== FALSE) {
    $summary.= t('Group by') . ': ' . $options[$settings['group_by']] . '<br>';
    $summary.= t('Group prefix text') . ': ' . $settings['group_prefix_text'] . '<br>';
    $summary.= t('Group connector text') . ': ' . $settings['grp_connector'] . '<br>';
  }

  return $summary;
}

/**
 * Implements hook_field_formatter_view().
 */
function taxo_matrix_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $raw_items, $display) {
  $element = array();

  if (!$raw_items) { //If there are no items to show (eg. no content saved for this field on this node) then hide the field
    return;
  }
  
  switch ($display['type']) {
    case 'taxo_matrix_grid':
		  $coldata = taxo_matrix_get_voc_details($field['settings']['col']);
		  $rowdata = taxo_matrix_get_voc_details($field['settings']['row']);

		  $cols_count = $coldata['count'];
		  $rows_count = $rowdata['count'];

      if (count($raw_items[0]) == 1) { //if a single value is passed in from views
        $element[0] = array(
          '#markup' => field_filter_xss($raw_items[0]['value']), 
        );
      }
      
      //transpose the $items array into a [$row][$col] = value arangement
      foreach ($raw_items as $delta => $item) {
        $items[$item['row']][$item['col']][] = taxo_matrix_cell_value($item, $field);
      }

      //work out how many rows and columns to show, which is the number of rows with at least some data in the row.
      foreach ($items as $row_id => $row) {
        foreach ($row as $col_id => $value) {
          if ($value != '') {
            $rows_count = $row_id;
          }
        }
      }

      if ($cols_count > 0) {
        $cols_count = $cols_count;
      }
      else {
        foreach ($items as $row_id => $row) {
          foreach ($row as $col_id => $value) {
            if (!empty($value)) {
              $max_cols[$col_id] = $col_id;
            }
          }
        }
        $cols_count = max($max_cols);
      }

      //populate the data part of the table rows
      $rows = array();
      for ($row = 1; $row <= $rows_count; $row++) {
        for ($col = 1; $col <= $cols_count; $col++) {
          $value = array('');
          if (isset($items[$row][$col])) {
            $value = $items[$row][$col];
          }
          $rows[$row][$col] = $value;
        }
      }
      
      //collapse any multi-valued results into a list
      foreach ($rows as $row_id => $row) {
        foreach ($row as $col_id => $values) {
	      	$rowVal = '';
	      	if ($values[0] == '0-0') {
	      		$rowVal = '<a class="taxo-term none">#</a>';
	      	}
	      	else {
	      		$rowVal = '<a class="taxo-term chosen">#</a>';
	      	}
          $rows[$row_id][$col_id] = array(
          	'data' => $rowVal, 
          	'class' => 'term-matrix-col-'. $col_id .' term-matrix-row-'. $row_id .' term-matrix-cell-'. $row_id .'-'. $col_id
        	);
        }
      }

		  $row_labels = $rowdata['labels'];
		  $headers = $coldata['labels'];
		  array_unshift($headers, ''); //cell 0,0

      for ($row = 1; $row <= $rows_count; $row++) {
      	$rowVal = '';
      	if ($row != '0-0') {
      		$rowVal = '<a class="taxo-term chosen">#</a>';
      	}
        $cell = array(
        	'data' => $rowVal,
        	'class' => 'taxo-matrix-col-header',
      	);
        array_unshift($rows[$row], array('data' => $row_labels[$row-1], 'class' => 'taxo-matrix-row-desc'));
      }

		  drupal_add_js(drupal_get_path('module', 'taxo_matrix') .'/js/taxo_matrix.js');
		  drupal_add_css(drupal_get_path('module', 'taxo_matrix') .'/css/taxo_matrix.css');

      $element[0] = array(
        '#theme' => 'taxo_matrix_output',
        '#header' => $headers,
        '#rows' => $rows,
      );
    	break;
    case 'taxo_matrix_list' :
		  $coldata = taxo_matrix_get_voc_details($field['settings']['col']);
		  $rowdata = taxo_matrix_get_voc_details($field['settings']['row']);

		  $cols_count = $coldata['count'];
		  $rows_count = $rowdata['count'];

		  $row_labels = $rowdata['labels'];
		  $col_labels = $coldata['labels'];

      if (count($raw_items[0]) == 1) { //if a single value is passed in from views
        $element[0] = array(
          '#markup' => field_filter_xss($raw_items[0]['value']), 
        );
      }
      
      //Grouping the data
      $items = array();
      switch ($display['settings']['group_by']) {
      	case 'row':
		      foreach ($raw_items as $delta => $item) {
		        $items[$row_labels[$item['row']-1]][$col_labels[$item['col']-1]][] = taxo_matrix_cell_value($item, $field);
		      }
      		break;
      	case 'col':
		      foreach ($raw_items as $delta => $item) {
		        $items[$col_labels[$item['col']-1]][$row_labels[$item['row']-1]][] = taxo_matrix_cell_value($item, $field);
		      }
      		break;
      }
      $list = array();
      foreach ($items as $grp => $flds) {
      	$grpvals = array();
      	foreach($flds as $name => $fld) {
      		if ($fld[0] != '0-0') {
      			$grpvals[] = $name;
      		}
      	}
      	if (count($grpvals) > 0) {
					$list[] = $display['settings']['group_prefix_text'] . $grp . $display['settings']['grp_connector'] . implode(', ', $grpvals);
      	}
      }
	    $element[0] = array(
	      '#theme' => 'taxo_matrix_list',
	      '#list' => $list,
	    );
    	break;
  }

  return $element;
}

/**
 * Find a value of a cell for either a string or a select list
 *
 * @param $item
 *    array representing the cell
 * @param $field
 *     array containing the field data
 * @return
 *     value to display
 */
function taxo_matrix_cell_value($item, $field) {
  $prefix = '';
  $suffix = '';
  $value = $item['row_value'] . '-' . $item['col_value'];
  return check_plain($prefix . $value . $suffix);
}

/**
 * Theme a set of form elements into a table
 */
function theme_taxo_matrix_table($variables) {
  $form = $variables['form'];
  $rows_count = $form['#taxo_matrix_rows'];
  $cols_count = $form['#taxo_matrix_cols'];
  $column_labels = $form['#column_labels'];
  $row_labels = $form['#row_labels'];
  $field_name = $form['#field_name'];

  $table_rows = array();
  for ($row = 1; $row <= $rows_count; $row++) {
    for ($col = 1; $col <= $cols_count; $col++) {
      $table_rows[$row - 1][$col - 1] = drupal_render($form['grid'][$row .'-'. $col]);
    }
  }
  
  //labels
  if (!empty($row_labels)) {
    array_unshift($column_labels, ''); //The first column will contain the label
    foreach($row_labels as $id => $label) {
      array_unshift($table_rows[$id], $label);
    }
  }

  $output  = '<div id="matrix-field-'. $field_name .'">';
  $output .= theme('form_element_label', (array('element' => $form)));
  $output .= theme('table', array('header' => $column_labels, 'rows' => $table_rows));
  $output .= filter_xss_admin($form['#description']);
  $output .= '</div>';
  return $output;
}

/**
 * Theme a set of elements into a table
 */
function theme_taxo_matrix_output($variables) {
	$out = theme('table', array('header' => $variables['header'], 'rows' => $variables['rows'], 'class' => array('taxo-matrix-field')));
	return $out;
}

/**
 * Theme a set of elements into a list
 */
function theme_taxo_matrix_list($variables) {
	$attributes = array('class' => 'taxo-matrix-field-list');
	$out = theme_item_list(array('items' => $variables['list'], 'title' => '', 'type' => 'ul', 'attributes' => $attributes));
	return $out;
}

/**
 * [taxo_matrix_get_labels description]
 */
function taxo_matrix_get_voc_details($vid) {
	$voc = array();
	$terms = taxonomy_get_tree($vid);
	$voc['count'] = count($terms);
	foreach ($terms as $term) {
		$voc['tids'][] = $term->tid;
		$voc['labels'][] = $term->name;
	};
	return $voc;
}